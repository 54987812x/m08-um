                    Curriculum
![Alt](/Images/pp.jpg "Title")

**Nombre: Uíliam Mateu Martins**

**Edad: 18**

**Dirección: Carrer San Josep, Hospitalet de Llobregat**

**Email: uiliampsa@gmail.com**

**Telefóno: 644342567**

## **Objetivo Profesional**

Desde pequeño tengo interés por la informática y estoy en busca de mi primer trabajo.
Me gustaría absorber todo lo necesario para formarme en esta área y para ello estoy deseando comenzar cuanto antes mi vida laboral.

## **Formación Académica**

2018/2019: Ciclo Formativo en Sistemas Microinformáticos y Redes (SMIX) 
Institut Poblenou Barcelona, España

2015: ESO - Colegio Boni Consilii. São Paulo. Brasil

## **Idiomas**

Portugués: Nativo

Español: Hablado y escrito

Catalán: Nivel B del CPNL

Inglés: Básico


## **Conocimientos Informáticos**

Paquete de LibreOffice nivel medio 

Programas específicos: Photoshop nivel básico, Packet Tracer nivel medio, Gimp nivel médio, Kdenlive nivel básico 

Lenguajes de programación: Python básico

Instalación y administración de sistemas operativos Windows y Linux

Certificado Linux Essentials [link](https://gitlab.com/54987812x/m08-um/blob/master/Images/LE-1.pdf "Title")

Conocimientos en Montaje de Ordenadores

## **Capacidades transversales**

* Trabajo en grupo
* Ganas de aprender nuevas cosas
* Rápida Adaptación
* Comprometido
* Responsable
* Flexible

## **Planes**

- [ ]Crear mi primero juego

- [ ]Decidir cual carrera seguir

- [ ]Continuar estudiando Informática
