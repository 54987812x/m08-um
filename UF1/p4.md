En aquesta nova pràctica explorarem diferents **Sistemes Operatius** online que tenen funcionalitats similars als que poden oferir els sistemes operatius d'escriptori.

És important:
- Explorar a Internet qualsevol punt que no sabem com funciona.
- Per a cada punt anar redactant l'informe que el professor avaluarà.
- Els punts en negreta són obligatoris que estiguin en l'informe.

Escull una de les següents activitats i documenta-la degudament en l’informe:
- [**Horbito. (Uiliam)**](https://www.horbito.com/) Sistema operatiu Online. Fes un compte i prova totes les funcionalitats que té a través de la web. Horbito

R- Horbito en el seu lloc web oficial, té aquest missatge:

"Debido a una serie de decisiones estratégicas, sentimos comunicarle que vamos a suspender las cuentas gratuitas en horbito.
Por favor, descargue todos sus ficheros antes del final de Octubre de 2019.
Superada esa fecha precederemos a cerrar su cuenta y eliminaremos toda la información relacionada con usted.
Sentimos las molestias."

Doncs, per això no té com crear una conta nova i testar-la

- [**Oodesk. (Uiliam)**](http://www.oodesk.com/) Sistema operatiu Online. Fes un compte i prova totes les funcionalitats que té a través de la web.

![Alt](/Images/pre.png "Title") 

Tenen dues opcions i pots ajudar-los fen una donació

Sense pagar tens 10GB de emmagatzematge


En procés d'inici del'Oodesk és molt senzill, després de crear una conta, pots entrar directament

![Alt](/Images/oodesk.png "Title")

Les eines són molt complicades i sense explicacions de com utilitzar-las, tenen poca varietat, només Full de càlcul i Editor de text i Presentacions.

No funciona l'editor de text, però pots posar arxius i veure'ns. Però no pots editar-los

Para posar arxius del teu ordinador en el sistema operatiu, necessites el Java actiu en el teu navegador

Hi ha una varietat d'aplicacions, però no funciona cap o funciona malament

Ni tot està traduït al castellà, algunes coses surten en francès

![Alt](/Images/fa.png "Title")

Un motiu és que està encara en la versió beta.

Aquest sistema operatiu està bé per l'emmagatzematge d'arxius i el seu 10 GB gratis, i la possibilitat que pots fàcilment tenir varies contes.