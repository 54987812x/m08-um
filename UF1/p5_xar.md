##  **1. Què és una LAN i quines diferències hi ha amb una WAN?**

LAN (Local Area Network), son redes locales, o sea, que abarca un área reducida a una casa, un departamento o un edificio.

Las principales diferencias son: el rango que una cubre, pues una es para áreas locales, o sean casas o pequeñas áreas. 

La WAN (Wide Area Network) funciona ya en un ámbito mayor, y no hace falta que estén conectados físicamente los ordenadores.


## **2. Busca quines són les capes del model OSI.**

7 - Nivel de Aplicación

6 - Nivel de Presentación 

5 - Nivel de Sesión 

4 - Nivel de Transporte 

3 - Nivel de Red

2 - Nivel de Enlace de Datos

1 - Nivel Físico

## **3. Quina és la unitat d'informació de les capes OSI: física, enllaç de dades, de xarxa i de transport.**

1. Fisica: Bit

2. Enlace de Datos: Trama

3. Red: Paquete

4. Transporte :TPDU

5. Sesión: SPDU

6. Presentación: PPDU

7. Aplicación: APDU  


## **4. Per a què serveix una adreça IP y una màscara de xarxa?**

La dirección ip, sirve para que se pueda reconocer un ordenador o dispositivo en una red.

Las máscaras de red, sirven para distinguir dentro de la dirección Ip, los bits que identifican a la red y el host

## **5. Explica les diferents classes d'adreçament IP: A, B, C. Indica per a cada classe el nombre de host que permet.**

La red classe A, tiene un segmento relleno con  1 a 126  y tiene una máscara de 255.0.0.0, hosts de WAN

La red classe B, tiene un segmento relleno con 128 a 191 y tiene una máscara de 255.255.0.0, hosts de MAN

La red classe c, tiene un segmento relleno con 192 a 223 y tiene una máscara de 255.255.255.0, hosts de LAN

## **6. Què són les classes D i E? Busca-les i explica-les.**

La IP de clase D está reservado exclusivamente para multi difusión. Las direcciones IP de esta clase va de 224.0.0.0 hasta 239.255.255.255. La clase D no tiene máscara de subred.

La IP de clase E está reservado para finalidades experimentales solo para R&D o estudio. Las direcciones IP de esta clase va de 240.0.0.0 a 255.255.255.254. Como Clase D, también esta clase no está equipada con máscara de subred.

## **7. Explica l’encapsulació dels paquets entre els diferents nivells. Per a què serveix?**

Sirve para gestionar de forma más eficiente los datos que se envían.

## **8. Quina diferència hi ha entre les adreces públiques i privades. Quines són les adreces privades**

La dirección pública es el identificador de nuestra red desde el exterior, o sea la de nuestro router de casa, que es  visible desde fuera, mientras que la privada es la que identifica a cada uno de los dispositivos conectados a nuestra red.

Las direcciones privadas son las que lo router asigna a nuestro ordenador, móvil o cualquier otro dispositivo que se esté conectado a él.

## **9. Què són les adreces unicast, broadcast i multicast? Quines diferències hi ha entre elles?**

Una dirección unicast está dirigida por uno emisor hasta un único receptor.

Una dirección broadcast está dirigida por uno emisor hasta todos los receptores posibles dentro de la red origen del host emisor.

Una dirección multicast está dirigida por uno emisor para varios receptores.

## **10. ¿Quines topologies de xarxa coneixes? 11.- Què és ARPANET?**

Topologias: 

1. p2p
2. Árbol
3. Bus 
4. Estrella 
5. Malla

ARPANET(Advanced Research Projects Agency Network) fue una red de computadoras creada por encargo del Departamento de Defensa de los Estados Unidos (DOD) para utilizarla como medio de comunicación entre las diferentes instituciones académicas y estatales.