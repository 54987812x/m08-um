# P1. Instal·lació i configuració d' Alfresco

## Instal·lació:

Amb l'ajuda de la documentació d'Alfresco hauràs d'instal·lar aquest gestor de fitxers a una màquina virtual.

* Fes una nova màquina virtual amb Ubuntu 18.04 LTS.
* La documentació per instal·lar d'Alfresco és: [Aquí](https://docs.alfresco.com/5.2/tasks/simpleinstall-enterprise-lin.html).

**Documentaràs** TOTS els passos que segueixis per instal·lar Alfresco (Amb explicacions i imatges).

![ESP](/UF2/imagenes/esp.png) he puesto alfresco en español.

![ESP](/UF2/imagenes/insta.png) he puesto en la versión avanzada para poder mirar los puertos.

![ESP](/UF2/imagenes/componenetes.png) he dejado los componentes padrón.

![ESP](/UF2/imagenes/lae.png) la carpeta de la instalación por defecto.

![ESP](/UF2/imagenes/base_de_datos.png) que puertos la base de datos (PostreSQL) irá utilizar.

![ESP](/UF2/imagenes/tomcat.png) configuración del puerto de tomcat.

![ESP](/UF2/imagenes/libreoffice.png) puerto de de libreoffice.

![ESP](/UF2/imagenes/ftpw.png) puerto ftp.

![ESP](/UF2/imagenes/ser.png) he activado los servicios.

Durant la instal·lació investiga quines tecnologies es fan servir i quins ports utilitza cada servei: **PostgreSQL, Java, Tomcat, etc...**, per exemple pots fer una taula amb: El nom de la tecnologia, el propòsit de la mateixa, la pàgina web oficial, el port utilitzat i els problemes que has hagut de solventar.


| PostreSQL   |  Java      	| Tomcat | Libreoffice | ftp |
|----------	|:-------------: | :-------------: | :-------------: | :-------------:	
| Puerto: 5432 | Puerto:  	| Puerto: 8080 | Puerto: 8100 | Puerto: 21 |
|PostgreSQL es un sistema de gestión de bases de datos relacional orientado a objetos y de código abierto|  |  Apache Tomcat funciona como un contenedor de servlets  | LibreOffice es un paquete de software de oficina libre y de código abierto  | DescripciónEl Protocolo de transferencia de archivos es un protocolo de red para la transferencia de archivos entre sistemas conectados a una red TCP |
|[Postgresql](https://www.postgresql.org/)|[Java](https://www.java.com/es/download/)|[Tomcat](http://tomcat.apache.org/)|[Libreoffice](https://es.libreoffice.org/)|

Documenta tot i explica-ho.

## Configuració:

* Videotutorials de configuració d'Alfresco: [Aquí](https://docs.alfresco.com/community/topics/alfresco-video-tutorials.html).

Una vegada has documentat tot el procés de instal·lació, hauràs de completar la següent secció, pensa que la empresa a la que has de donar suport és: Weyland Yutani, OCP, Team Rocket, ConSec o Hydra.

Segueix els videotutorials per tal d'ajudar-te amb els següents passos i documentals.

1. Personalitza la teva **MAIN PAGE** amb els *COMPONENTS* necessaris, fes-ho de la manera que creguis que serà més còmoda i mostra el resultat final amb una captura.

![ESP](/UF2/imagenes/alfresco.png)

2. Actualitza la **personal information** del teu perfil, incloent informació, fotos i comptes de xarxes socials.

![ESP](/UF2/imagenes/perfil.png)

3. Crea tres **sites** que donin cobertura als diferents nivells de visibilitat. A cada **site** donaràs un nom explicatiu del que conté i fes un **site** de cada manera: **public, moderate and private**. Com faràs que els usuaris accedeixin al **moderate site**? Com entrarà un usuari a un **site**? Quins perfils hi ha?

![ESP](/UF2/imagenes/moderate.png) en moderate tienes que pedir al administrador acceso

![ESP](/UF2/imagenes/sites.png) Calendario = Moderate, Redterce = Privado, Reuniones = Público.

perfiles = Administrador, Usuario 


4. Personalitza 3 **sites** amb diferents columnes cadasqun. Posa la informació important a cada **site**. Què són els **Dashlets**? Quis **Dashlets** diferents pots incloure a un **site**?

Dashlets: son miniaplicaciones, que te permiten ver ciertas informaciones sobre la página, como wiki, calendario, sitios, tareas, etc...

5. Afegeix diferents **components** a cada **site**. Afegeix calendari, wiki i algunes altres característiques a cada **site**.



6. Quin **profile** he de tindre per poder canviar la informació del **profile site**?

Administrador

7. Ara treu una de les pestanyes **tabs** que tens a un **site** i mostra el resultat abans i després.

![ESP](/UF2/imagenes/antes.png)

![ESP](/UF2/imagenes/depois.png)


8. Crea dins del **Document Library** tres carpetes **folders** per als propers plans de la teva empresa, al primer **folder** posa tres tipus diferents de fitxers, guardals i possa **tags** per tal de que puguin ser accesibles per cerca. Al segon arrossega 3 fitxers a dins. Al tercer puja **upload** tres fitxers que vulguis.

9. Cerca els fitxers per els **tags** que has fet al punt anterior.

10. Edita un dels fitxers anteriors amb **Google Docs**, desa una versió del document i afegeix un comentari amb els canvis efectuats.

11. Crea 5 usuaris.

![ESP](/UF2/imagenes/usuari.png)

12. Afegeix múltiples usuaris mitjançant **CSV**, documenta el procés amb captures.

13. Assigna els usuaris a un dels **sites**, amb diferents **profiles**: **collaborator, contributor i consumer**. Quin tipus de **permissos** té cadasqun d'ells respecte a un fitxer?

14. **Version Files**. Modifica un fitxer existent i desa'l com a **minor change**. Després carregal i modifica'l MOLT amb un usuari que pugui fer-ho. Desa'l coma  **major change**. Finalment, torna a l'usuari original, mira la versió original del document i crea una nova versió desfent els canvis. És útil aquesta funcionalitat? Per què?

15. Ara crea un grup on afegiràs els 5 usuaris anteriors. Al segon **site**, selecciona un fitxer del **Document Library** i inclou tots els possibles permisos a aquest grup.

16. A tasques **Tasks -> My Tasks** trobaràs com crear un **Workflow** per al grup de treball **workgroup**. Crea una tasca per a cada membre del grup. Per a què serveix el workflow?

17. A una carpeta **folder** crea una nova norma **rule** que consisteixi en enviar un email a tots els membres del grup cada vegada que un document es crea o s'afegeix a un **folder**.
